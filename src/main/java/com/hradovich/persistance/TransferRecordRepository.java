package com.hradovich.persistance;

import com.hradovich.model.TransferRecord;
import com.hradovich.model.TransferRecordType;
import com.hradovich.virtual.VirtualDocumentDatabase;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class TransferRecordRepository {
    private static TransferRecordRepository instance;
    private VirtualDocumentDatabase<TransferRecord> transferRecordById;
    private AtomicLong nextTransferId;

    public static synchronized TransferRecordRepository getInstance() {
        if (instance == null) {
            instance = new TransferRecordRepository();
        }
        return instance;
    }

    public void reset() {
        instance = null;
    }

    private TransferRecordRepository() {
        this.transferRecordById = new VirtualDocumentDatabase<>();
        this.nextTransferId = new AtomicLong(0);
    }

    public void addMoneyTransferRecord(String accountLeft, String accountRight,
                                       TransferRecordType recordType, BigInteger amount) {
        synchronized (this) {
            TransferRecord record = new TransferRecord(
                    recordType,
                    accountLeft,
                    accountRight,
                    amount,
                    Calendar.getInstance().getTimeInMillis());

            transferRecordById.insertRow(
                    String.valueOf(nextTransferId.incrementAndGet()),
                    record);
            System.out.println(recordType + " " +
                    accountLeft + "->" + accountRight + " [" + amount + "$] " +
                    record.getTimestamp());
        }
    }

    public TransferRecord readRecord(Long transferId) {
        return transferRecordById.getRow(String.valueOf(transferId));
    }

    public List<TransferRecord> readAllRecords() {
        List<TransferRecord> recordList = new LinkedList<>();
        for(int i = 1; i <= nextTransferId.intValue(); i++) {
            recordList.add(
                    transferRecordById.getRow(String.valueOf(i))
            );
        }

        return recordList;
    }
}
