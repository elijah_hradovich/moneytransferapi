package com.hradovich.persistance;

import com.hradovich.virtual.VirtualDocumentDatabase;
import javafx.util.Pair;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class BankAccountsRepository {
    private static BankAccountsRepository instance;
    private final List<String> accounts = Arrays.asList("a00", "b00", "c00");
    private VirtualDocumentDatabase<BigInteger> accountValueByNumberDatabase;

    public static synchronized BankAccountsRepository getInstance() {
        if (instance == null) {
            instance = new BankAccountsRepository();
        }
        return instance;
    }

    public void reset() {
        instance = null;
    }

    public BankAccountsRepository() {
        this.accountValueByNumberDatabase = new VirtualDocumentDatabase<>();
        initializeHardcodedData();
    }

    public boolean exists(String accountNumber) {
        return !Objects.isNull(
                accountValueByNumberDatabase.getRow(accountNumber));
    }

    public BigInteger getAccountStatus(String accountNumber) {
        return accountValueByNumberDatabase.getRow(accountNumber);
    }

    public List<Pair<String, Long>> getAllAccounts() {
        List<Pair<String, Long>> accountsStatuses = new LinkedList<>();
        accounts.forEach(a -> accountsStatuses
                .add(new Pair<>(a, getAccountStatus(a).longValue())));
        return accountsStatuses;
    }

    private void initializeHardcodedData() {
        accounts.forEach(a -> accountValueByNumberDatabase.insertRow(a, BigInteger.valueOf(1000000)));
    }
}
