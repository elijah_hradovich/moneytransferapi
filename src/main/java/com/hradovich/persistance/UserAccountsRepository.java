package com.hradovich.persistance;

import com.hradovich.virtual.VirtualDocumentDatabase;
import javafx.util.Pair;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class UserAccountsRepository {
    private static UserAccountsRepository instance;
    private final List<String> accounts = Arrays.asList("a01", "a02", "b01", "c01");
    private VirtualDocumentDatabase<BigInteger> accountValueByNumberDatabase;

    public static synchronized UserAccountsRepository getInstance() {
        if (instance == null) {
            instance = new UserAccountsRepository();
        }
        return instance;
    }

    public void reset() {
        instance = null;
    }

    public UserAccountsRepository() {
        this.accountValueByNumberDatabase = new VirtualDocumentDatabase<>();
        initializeHardcodedData();
    }

    public boolean exists(String accountNumber) {
        return !Objects.isNull(
                accountValueByNumberDatabase.getRow(accountNumber));
    }

    public void debit(String accountNumber, BigInteger amount) {
        BigInteger accountState = accountValueByNumberDatabase.getRow(accountNumber);
        accountValueByNumberDatabase.insertRow(
                accountNumber,
                accountState.subtract(amount));
    }

    public void credit(String accountNumber, BigInteger amount) {
        BigInteger accountState = accountValueByNumberDatabase.getRow(accountNumber);
        accountValueByNumberDatabase.insertRow(
                accountNumber,
                accountState.add(amount));
    }

    public BigInteger getAccountStatus(String accountNumber) {
        return accountValueByNumberDatabase.getRow(accountNumber);
    }

    public List<Pair<String, Long>> getAllAccounts() {
        List<Pair<String, Long>> accountsStatuses = new LinkedList<>();
        accounts.forEach(a -> accountsStatuses
                .add(new Pair<>(a, getAccountStatus(a).longValue())));
        return accountsStatuses;
    }

    private void initializeHardcodedData() {
        accounts.forEach(a -> accountValueByNumberDatabase.insertRow(a, BigInteger.valueOf(1000)));
    }
}
