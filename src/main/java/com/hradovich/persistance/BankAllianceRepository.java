package com.hradovich.persistance;

import com.hradovich.virtual.VirtualDocumentDatabase;

import java.util.*;

public class BankAllianceRepository {
    private static BankAllianceRepository instance;
    private VirtualDocumentDatabase<HashSet<String>> alliedBankAccounts;

    public static synchronized BankAllianceRepository getInstance() {
        if (instance == null) {
            instance = new BankAllianceRepository();
        }
        return instance;
    }

    public void reset() {
        instance = null;
    }

    public BankAllianceRepository() {
        this.alliedBankAccounts = new VirtualDocumentDatabase<>();
        initializeHardcodedData();
    }

    public Set<String> getAlliedBankAccounts(String bankAccount) {
        return alliedBankAccounts.getRow(bankAccount);
    }

    private void initializeHardcodedData() {
        //a00 -> b00; a00 -> c00
        //b00 -> a00
        List<String> accounts = Arrays.asList("a00", "b00", "c00");
        accounts.forEach(a -> alliedBankAccounts.insertRow(a, new HashSet<>()));
        alliedBankAccounts.getRow("a00").add("b00");
        alliedBankAccounts.getRow("a00").add("c00");
        alliedBankAccounts.getRow("b00").add("a00");
    }
}
