package com.hradovich.persistance;

import com.hradovich.model.MoneyTransfer;
import com.hradovich.model.TransferType;
import com.hradovich.virtual.VirtualDistributedQueue;
import com.hradovich.virtual.VirtualDistributedQueueException;

public class MoneyTransferQueue {
    private static volatile MoneyTransferQueue instance;
    private VirtualDistributedQueue<MoneyTransfer> queue;
    private static final Object mutex = new Object();

    public static MoneyTransferQueue getInstance() {
        MoneyTransferQueue result = instance;
        if (result == null) { //avoid calling volatile variable most of the time
            synchronized (mutex) {
                result = instance;
                if (result == null) {
                    result = instance = new MoneyTransferQueue();
                }
            }
        }
        return result;
    }

    public void reset() {
        instance = null;
    }

    public MoneyTransferQueue() {
        queue = new VirtualDistributedQueue<>(TransferType.getAllValuesAsString());
        System.out.println("Distributed queue initialized");
    }

    public void push(String topicName, MoneyTransfer transfer) {
        try {
            queue.push(topicName, transfer);
        } catch (VirtualDistributedQueueException e) {
            e.printStackTrace();
        }
    }

    public MoneyTransfer pull(String topicName) {
        try {
            return queue.pull(topicName);
        } catch (VirtualDistributedQueueException e) {
            e.printStackTrace();
        }

        return null;
    }
}
