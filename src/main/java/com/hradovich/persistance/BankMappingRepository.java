package com.hradovich.persistance;

import com.hradovich.virtual.VirtualDocumentDatabase;

import java.util.Arrays;
import java.util.List;

public class BankMappingRepository {
    private static BankMappingRepository instance;
    private VirtualDocumentDatabase<String> bankAccountByPrefix;

    public static synchronized BankMappingRepository getInstance() {
        if (instance == null) {
            instance = new BankMappingRepository();
        }
        return instance;
    }

    public void reset() {
        instance = null;
    }

    public BankMappingRepository() {
        this.bankAccountByPrefix = new VirtualDocumentDatabase<>();
        initializeHardcodedData();
    }

    public String getBankAccountByAccount(String account) {
        return bankAccountByPrefix.getRow(
                account.substring(0, 1)
        );
    }

    private void initializeHardcodedData() {
        List<String> prefix = Arrays.asList("a", "b", "c");
        List<String> accounts = Arrays.asList("a00", "b00", "c00");

        for (int i = 0; i < prefix.size(); i++) {
            bankAccountByPrefix.insertRow(prefix.get(i), accounts.get(i));
        }
    }
}
