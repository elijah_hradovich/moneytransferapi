package com.hradovich.persistance;

import com.hradovich.processing.ITransferProcessor;

public class MoneyTransferTopicListener extends Thread {
    private ITransferProcessor processor;

    public MoneyTransferTopicListener(ITransferProcessor processor) {
        this.processor = processor;
    }

    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()) {
            try {
                processor.processTransfer(
                        MoneyTransferQueue.getInstance()
                                .pull(processor.getSupportedTransferType().name()));

                //dynamic timeout would be better
                sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
