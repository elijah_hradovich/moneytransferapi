package com.hradovich;

import act.Act;

public class ApplicationEntry {

    public static void main(String[] args) throws Exception {
        Act.start("Money Transfer API");
    }
}
