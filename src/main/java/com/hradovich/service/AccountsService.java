package com.hradovich.service;

import act.util.JsonView;
import com.hradovich.persistance.BankAccountsRepository;
import com.hradovich.persistance.UserAccountsRepository;
import javafx.util.Pair;
import org.osgl.mvc.annotation.GetAction;

import java.util.List;

public class AccountsService {

    @GetAction("/user_accounts")
    @JsonView
    public List<Pair<String, Long>> getAllUserAccounts() {
        return UserAccountsRepository.getInstance()
                .getAllAccounts();
    }

    @GetAction("/bank_accounts")
    @JsonView
    public List<Pair<String, Long>> getAllBankAccounts() {
        return BankAccountsRepository.getInstance()
                .getAllAccounts();
    }
}
