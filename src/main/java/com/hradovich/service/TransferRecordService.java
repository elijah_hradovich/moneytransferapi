package com.hradovich.service;

import act.util.JsonView;
import com.hradovich.model.TransferRecord;
import com.hradovich.persistance.TransferRecordRepository;
import org.osgl.mvc.annotation.GetAction;

import java.util.List;

public class TransferRecordService {

    @GetAction("/records")
    @JsonView
    public List<TransferRecord> getAllRecords() {
        return TransferRecordRepository.getInstance()
                .readAllRecords();
    }
}
