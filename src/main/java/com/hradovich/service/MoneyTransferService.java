package com.hradovich.service;

import act.controller.Controller;
import com.hradovich.controller.MoneyTransferController;
import com.hradovich.controller.UserAccountsController;
import com.hradovich.model.MoneyTransfer;
import com.hradovich.model.TransferType;
import com.hradovich.persistance.MoneyTransferQueue;
import com.hradovich.persistance.MoneyTransferTopicListener;
import com.hradovich.processing.ITransferProcessor;
import com.hradovich.processing.InterBankOneHopTransferProcessor;
import com.hradovich.processing.InterBankTwoHopTransferProcessor;
import com.hradovich.processing.IntraBankTransferProcessor;
import org.osgl.mvc.annotation.PostAction;
import org.osgl.mvc.result.Result;

import java.util.Arrays;
import java.util.List;

public class MoneyTransferService {
    private MoneyTransferController transferController;
    private UserAccountsController userAccountsController;


    public MoneyTransferService() {
        transferController = new MoneyTransferController();
        userAccountsController = new UserAccountsController();
    }

    public MoneyTransferService(MoneyTransferController transferController,
                                UserAccountsController userAccountsController) {
        this.transferController = transferController;
        this.userAccountsController = userAccountsController;
    }

    @PostAction("/init")
    //due to framework limitations they are not initialized from the main method
    public Result initListeners() {
        List<ITransferProcessor> transferProcessors = Arrays.asList(
                new IntraBankTransferProcessor(),
                new InterBankOneHopTransferProcessor(),
                new InterBankTwoHopTransferProcessor());

        transferProcessors.forEach(
                processor -> {
                    MoneyTransferTopicListener listener = new MoneyTransferTopicListener(processor);
                    listener.start();
                }
        );
        return Controller.Util.OK_JSON;
    }

    @PostAction("/transfer")
    public Result transfer(Long amount, String sender, String receiver) {
        //Space for transfer object being end-to-end encrypted:
        //A place where it should be decrypted
        MoneyTransfer transfer = new MoneyTransfer(amount, sender, receiver);

        if(!transferController.checkIfValid(transfer)) {
            //The framework actually probably not of the best choice
            //No easy way to return BAD REQUEST 400
            return Controller.Util.NO_CONTENT;
        }

        try {
            MoneyTransferQueue.getInstance()
                    .push(TransferType.INTRA_BANK.name(), transfer);
            userAccountsController
                    .updateUserAccountStatesByTransfer(transfer);
            System.out.println("/transfer: " + transfer.getSenderAccount() + "->"
                    + transfer.getReceiverAccount());
        } catch(Exception e) {
            //Should be 500
            return Controller.Util.NO_CONTENT;
        }

        return Controller.Util.OK_JSON;
    }
}
