package com.hradovich.processing;

import com.hradovich.model.MoneyTransfer;
import com.hradovich.model.TransferRecordType;
import com.hradovich.model.TransferType;
import com.hradovich.persistance.MoneyTransferQueue;
import com.hradovich.persistance.TransferRecordRepository;

import java.util.Objects;

public class IntraBankTransferProcessor implements ITransferProcessor {

    @Override
    public void processTransfer(MoneyTransfer transfer) {
        if (Objects.isNull(transfer))
            return;

        if(accountsFromTheSameBank(
                transfer.getSenderAccount(),
                transfer. getReceiverAccount())) {
            System.out.println("INTRA BANK PROCESSOR: new transfer");
            createTransferRecords(transfer);
        } else {
            System.out.println("INTRA BANK PROCESSOR: not Intra type, push to next layer");
            MoneyTransferQueue.getInstance()
                    .push(TransferType.INTER_BANK_ONE_HOP.name(), transfer);
        }

    }

    @Override
    public TransferType getSupportedTransferType() {
        return TransferType.INTRA_BANK;
    }

    private void createTransferRecords(MoneyTransfer transfer) {
        TransferRecordRepository.getInstance()
                .addMoneyTransferRecord(
                        transfer.getSenderAccount(),
                        transfer.getReceiverAccount(),
                        TransferRecordType.DEBIT,
                        transfer.getAmount()
                );
        TransferRecordRepository.getInstance()
                .addMoneyTransferRecord(
                        transfer.getReceiverAccount(),
                        transfer.getSenderAccount(),
                        TransferRecordType.CREDIT,
                        transfer.getAmount()
                );
    }

    //currently hardcoded logic
    //the idea is that some bank-defining prefix of the account is the same
    //probably there is a separate repository for it
    private boolean accountsFromTheSameBank(String account1, String account2) {
        return !Objects.isNull(account1) && !Objects.isNull(account2) &&
                account1.substring(0, 1).equals(account2.substring(0, 1));
    }
}
