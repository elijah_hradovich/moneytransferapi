package com.hradovich.processing;

import com.hradovich.model.MoneyTransfer;
import com.hradovich.model.TransferRecordType;
import com.hradovich.model.TransferType;
import com.hradovich.persistance.*;

import java.util.Objects;
import java.util.Optional;

public class InterBankTwoHopTransferProcessor implements ITransferProcessor {

    @Override
    public void processTransfer(MoneyTransfer transfer) {
        if(Objects.isNull(transfer))
            return;

        String bankOfSender = BankMappingRepository.getInstance()
                .getBankAccountByAccount(transfer.getSenderAccount());
        String bankOfReceiver = BankMappingRepository.getInstance()
                .getBankAccountByAccount(transfer.getReceiverAccount());

        Optional<String> intermediatoryBank = BankAllianceRepository.getInstance()
                .getAlliedBankAccounts(bankOfSender)
                .stream()
                .filter(ally -> !ally.contains(bankOfReceiver))
                .findFirst();

        if(intermediatoryBank.isPresent()) {
            System.out.println("INTER BANK PROCESSOR TWO HOP: new transfer");
            createTransferRecords(transfer, intermediatoryBank.get(),
                    bankOfSender, bankOfReceiver);
        } else {
            System.out.println("INTER BANK PROCESSOR TWO HOP: not that type, push to next layer");
            MoneyTransferQueue.getInstance()
                    .push(TransferType.INTERNATIONAL.name(), transfer);
        }
    }

    @Override
    public TransferType getSupportedTransferType() {
        return TransferType.INTER_BANK_TWO_HOPS;
    }

    private void createTransferRecords(MoneyTransfer transfer, String intemediatoryBank,
                                       String bankOfSender, String bankOfReceiver) {
        TransferRecordRepository.getInstance()
                .addMoneyTransferRecord(
                        bankOfSender,
                        transfer.getSenderAccount(),
                        TransferRecordType.DEBIT,
                        transfer.getAmount());
        TransferRecordRepository.getInstance()
                .addMoneyTransferRecord(
                        intemediatoryBank,
                        bankOfSender,
                        TransferRecordType.DEBIT,
                        transfer.getAmount());
        TransferRecordRepository.getInstance()
                .addMoneyTransferRecord(
                        intemediatoryBank,
                        bankOfReceiver,
                        TransferRecordType.CREDIT,
                        transfer.getAmount());
        TransferRecordRepository.getInstance()
                .addMoneyTransferRecord(
                        bankOfReceiver,
                        transfer.getReceiverAccount(),
                        TransferRecordType.CREDIT,
                        transfer.getAmount());
    }
}
