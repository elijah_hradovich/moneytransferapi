package com.hradovich.processing;

import com.hradovich.model.MoneyTransfer;
import com.hradovich.model.TransferType;

public interface ITransferProcessor {
    void processTransfer(MoneyTransfer transfer);
    TransferType getSupportedTransferType();
}
