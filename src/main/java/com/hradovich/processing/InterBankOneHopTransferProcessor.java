package com.hradovich.processing;

import com.hradovich.model.MoneyTransfer;
import com.hradovich.model.TransferRecordType;
import com.hradovich.model.TransferType;
import com.hradovich.persistance.BankAllianceRepository;
import com.hradovich.persistance.BankMappingRepository;
import com.hradovich.persistance.MoneyTransferQueue;
import com.hradovich.persistance.TransferRecordRepository;

import java.util.Objects;

public class InterBankOneHopTransferProcessor implements ITransferProcessor {

    @Override
    public void processTransfer(MoneyTransfer transfer) {
        if(Objects.isNull(transfer))
            return;

        String bankOfSender = BankMappingRepository.getInstance()
                .getBankAccountByAccount(transfer.getSenderAccount());
        String bankOfReceiver = BankMappingRepository.getInstance()
                .getBankAccountByAccount(transfer.getReceiverAccount());

        if(senderBankIsAlliedToReceiverInOneHop(bankOfSender, bankOfReceiver)) {
            System.out.println("INTER BANK PROCESSOR ONE HOP: new transfer");
            createTransferRecords(transfer, bankOfSender, bankOfReceiver);
        } else {
            System.out.println("INTER BANK PROCESSOR ONE HOP: not that type, push to next layer");
            MoneyTransferQueue.getInstance()
                    .push(TransferType.INTER_BANK_TWO_HOPS.name(), transfer);
        }
    }

    @Override
    public TransferType getSupportedTransferType() {
        return TransferType.INTER_BANK_ONE_HOP;
    }

    private void createTransferRecords(MoneyTransfer transfer,
                                       String bankOfSender, String bankOfReceiver) {
        TransferRecordRepository.getInstance()
                .addMoneyTransferRecord(
                        bankOfSender,
                        transfer.getSenderAccount(),
                        TransferRecordType.DEBIT,
                        transfer.getAmount());
        TransferRecordRepository.getInstance()
                .addMoneyTransferRecord(
                        bankOfSender,
                        bankOfReceiver,
                        TransferRecordType.CREDIT,
                        transfer.getAmount());
        TransferRecordRepository.getInstance()
                .addMoneyTransferRecord(
                        bankOfReceiver,
                        transfer.getReceiverAccount(),
                        TransferRecordType.CREDIT,
                        transfer.getAmount());
    }

    private boolean senderBankIsAlliedToReceiverInOneHop(String bankAccount1, String bankAccount2) {
        return BankAllianceRepository.getInstance()
                .getAlliedBankAccounts(bankAccount1)
                .contains(bankAccount2);
    }
}
