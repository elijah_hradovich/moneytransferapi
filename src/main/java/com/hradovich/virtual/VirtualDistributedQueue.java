package com.hradovich.virtual;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class VirtualDistributedQueue<T> {
    private Map<String, Queue<T>> virtualTopicQueueMap;

    public VirtualDistributedQueue(List<String> topicList) {
        virtualTopicQueueMap = new HashMap<String, Queue<T>>();
        initializeTopics(topicList);
    }

    public void push(String topicName, T entity) throws VirtualDistributedQueueException {
        synchronized (this) {
            if (!virtualTopicQueueMap.containsKey(topicName)) {
                throw new VirtualDistributedQueueException();
            }

            System.out.println("Queue size increase: " + topicName +
                    " " + (virtualTopicQueueMap.get(topicName).size() + 1));
            virtualTopicQueueMap.get(topicName).add(entity);
        }
    }

    public T pull(String topicName) throws VirtualDistributedQueueException {
        synchronized (this) {
            if (!virtualTopicQueueMap.containsKey(topicName)) {
                throw new VirtualDistributedQueueException();
            }

            //real life solution does not remove the head
            //but it is a model with reduced capabilities of real system
            if (virtualTopicQueueMap.get(topicName).size() > 0)
                System.out.println("Queue size decrease: " + topicName +
                        " " + (virtualTopicQueueMap.get(topicName).size() - 1));
            return virtualTopicQueueMap.get(topicName).poll();
        }
    }

    public Set<String> getTopicNames() {
        return virtualTopicQueueMap.keySet();
    }

    private void initializeTopics(List<String> topicList) {
        synchronized (this) {
            topicList = (topicList == null) ? new LinkedList<>() : topicList;

            topicList.forEach(topic -> {
                virtualTopicQueueMap.put(topic, new ConcurrentLinkedQueue<>());
            });
        }
    }
}
