package com.hradovich.virtual;

import java.util.concurrent.ConcurrentHashMap;

public class VirtualDocumentDatabase<T> {
    private ConcurrentHashMap<String, T> database;

    public VirtualDocumentDatabase() {
        database = new ConcurrentHashMap<>();
    }

    public T getRow(String key) {
        return database.get(key);
    }

    public void insertRow(String key, T value) {
        database.put(key, value);
    }

    public void deleteRow(String key) {
        database.remove(key);
    }
}
