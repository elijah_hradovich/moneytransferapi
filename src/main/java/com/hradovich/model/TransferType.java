package com.hradovich.model;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum TransferType {
    INTRA_BANK,
    INTER_BANK_ONE_HOP,
    INTER_BANK_TWO_HOPS,
    INTERNATIONAL;

    public static List<String> getAllValuesAsString() {
        return Stream.of(TransferType.values())
                .map(TransferType::name)
                .collect(Collectors.toList());
    }
}
