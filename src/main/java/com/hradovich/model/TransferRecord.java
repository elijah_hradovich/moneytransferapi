package com.hradovich.model;

import java.math.BigInteger;

public class TransferRecord {
    private TransferRecordType recordType;
    private String left;
    private String right;
    private BigInteger amount;
    private Long timestamp;

    public TransferRecord(TransferRecordType recordType, String left, String right,
                          BigInteger amount, Long timestamp) {
        this.recordType = recordType;
        this.left = left;
        this.right = right;
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public TransferRecordType getRecordType() {
        return recordType;
    }

    public String getLeft() {
        return left;
    }

    public String getRight() {
        return right;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public Long getTimestamp() {
        return timestamp;
    }
}
