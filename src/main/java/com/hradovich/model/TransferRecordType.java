package com.hradovich.model;

public enum TransferRecordType {
    DEBIT,
    CREDIT
}
