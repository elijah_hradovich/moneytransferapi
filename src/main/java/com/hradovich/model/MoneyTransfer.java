package com.hradovich.model;

import java.math.BigInteger;

public class MoneyTransfer {
    private BigInteger amount;
    private String senderAccount;
    private String receiverAccount;
    //metadata fields (address etc) are not relevant for the prototype itself

    public MoneyTransfer() {
    }

    public MoneyTransfer(Long amount, String senderAccount, String receiverAccount) {
        this.amount = BigInteger.valueOf(amount);
        this.senderAccount = senderAccount;
        this.receiverAccount = receiverAccount;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public String getSenderAccount() {
        return senderAccount;
    }

    public String getReceiverAccount() {
        return receiverAccount;
    }
}
