package com.hradovich.controller;

import com.hradovich.model.MoneyTransfer;
import com.hradovich.persistance.BankAccountsRepository;
import com.hradovich.persistance.BankMappingRepository;

import java.util.Objects;

public class MoneyTransferController {
    private static MoneyTransferController instance;

    public static synchronized MoneyTransferController getInstance() {
        if (instance == null) {
            instance = new MoneyTransferController();
        }
        return instance;
    }

    public boolean checkIfValid(MoneyTransfer transfer) {
        //check account numbers, sender/receiver information
        //is simplified here
        if(!Objects.isNull(transfer) &&
                !Objects.isNull(transfer.getAmount()) &&
                !Objects.isNull(transfer.getSenderAccount()) &&
                !Objects.isNull(transfer.getReceiverAccount())) {

            String bankOfSender = BankMappingRepository.getInstance()
                    .getBankAccountByAccount(transfer.getSenderAccount());
            String bankOfReceiver = BankMappingRepository.getInstance()
                    .getBankAccountByAccount(transfer.getReceiverAccount());

            return BankAccountsRepository.getInstance().exists(bankOfSender) &&
                    BankAccountsRepository.getInstance().exists(bankOfReceiver);
        }

        return false;
    }
}
