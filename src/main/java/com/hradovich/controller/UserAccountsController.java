package com.hradovich.controller;

import com.hradovich.model.MoneyTransfer;
import com.hradovich.persistance.UserAccountsRepository;

import java.math.BigInteger;

public class UserAccountsController {
    private static UserAccountsController instance;

    public static synchronized UserAccountsController getInstance() {
        if (instance == null) {
            instance = new UserAccountsController();
        }
        return instance;
    }

    public void updateUserAccountStatesByTransfer(MoneyTransfer transfer) {
        debitUserAccountIfExists(
                transfer.getSenderAccount(),
                transfer.getAmount());
        creditUserAccountIfExists(
                transfer.getReceiverAccount(),
                transfer.getAmount());
    }

    private void creditUserAccountIfExists(String userAccount, BigInteger amount) {
        if (UserAccountsRepository.getInstance()
                .exists(userAccount)) {
            UserAccountsRepository.getInstance()
                    .credit(
                            userAccount,
                            amount);
        }
    }

    private void debitUserAccountIfExists(String userAccount, BigInteger amount) {
        if (UserAccountsRepository.getInstance()
                .exists(userAccount)) {
            UserAccountsRepository.getInstance()
                    .debit(
                            userAccount,
                            amount);
        }
    }
}
