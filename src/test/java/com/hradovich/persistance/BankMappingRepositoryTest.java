package com.hradovich.persistance;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BankMappingRepositoryTest {

    @Test
    public void getBankAccountByPrefixTest_positive() {
        assertEquals("a00",
                BankMappingRepository
                        .getInstance()
                        .getBankAccountByAccount("a01"));
    }

    @Test
    public void getBankAccountByPrefixTest_noSuchPrefix() {
        assertNull(BankMappingRepository
                        .getInstance()
                        .getBankAccountByAccount("d01"));
    }
}
