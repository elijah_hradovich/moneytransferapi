package com.hradovich.persistance;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BankAccountsRepositoryTest {

    @Before
    public void before() {
        BankAccountsRepository.getInstance().reset();
    }

    @Test
    public void getAccountStatusTest_positive() {
        assertEquals(
                BigInteger.valueOf(1000000),
                BankAccountsRepository
                        .getInstance()
                        .getAccountStatus("a00"));
    }

    @Test
    public void getAccountStatusTest_noSuchPrefix() {
        assertNull(BankAccountsRepository
                        .getInstance()
                        .getAccountStatus("d00"));
    }
}
