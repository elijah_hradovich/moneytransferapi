package com.hradovich.persistance;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UserAccountsRepositoryTest {

    @Before
    public void before() {
        UserAccountsRepository.getInstance().reset();
    }

    @Test
    public void debitTest_positive() {
        UserAccountsRepository
            .getInstance()
            .debit("a01", BigInteger.valueOf(20));

        assertEquals(
                BigInteger.valueOf(980),
                UserAccountsRepository
                        .getInstance()
                        .getAccountStatus("a01"));

    }

    @Test
    public void creditTest_positive() {
        UserAccountsRepository
                .getInstance()
                .credit("a01", BigInteger.valueOf(20));

        assertEquals(
                BigInteger.valueOf(1020),
                UserAccountsRepository
                        .getInstance()
                        .getAccountStatus("a01"));

    }

    @Test
    public void getAccountStatusTest_positive() {
        assertEquals(
                BigInteger.valueOf(1000),
                UserAccountsRepository
                        .getInstance()
                        .getAccountStatus("a01"));
    }

    @Test
    public void getAccountStatusTest_noSuchPrefix() {
        assertNull(UserAccountsRepository
                        .getInstance()
                        .getAccountStatus("d01"));
    }

    //handle negative complex cases for credit/debit
    //i.e. acknowledgement of successful operation so it is not assigned second time
}
