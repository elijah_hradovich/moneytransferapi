package com.hradovich.persistance;

import com.hradovich.model.TransferRecord;
import com.hradovich.model.TransferRecordType;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class TransferRecordRepositoryTest {
    private TransferRecordRepository repository = TransferRecordRepository.getInstance();

    @Before
    public void before() {
        repository.reset();
    }

    @Test
    public void addAndGetMoneyTransferRecordTest_positive() {
        repository.addMoneyTransferRecord("a01", "a02",
                TransferRecordType.DEBIT,
                BigInteger.valueOf(20));

        TransferRecord record = repository.readRecord(1L);

        assertNotNull(record);
        assertNotNull(record.getTimestamp());
        assertEquals("a01", record.getLeft());
        assertEquals("a02", record.getRight());
        assertEquals(TransferRecordType.DEBIT, record.getRecordType());
        assertEquals(BigInteger.valueOf(20), record.getAmount());
    }

    @Test
    public void readRecord_null() {
        assertNull(repository.readRecord(1L));
    }
}
