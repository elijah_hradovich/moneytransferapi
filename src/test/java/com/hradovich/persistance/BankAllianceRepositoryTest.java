package com.hradovich.persistance;

import org.junit.Test;

import static org.junit.Assert.*;

public class BankAllianceRepositoryTest {

    @Test
    public void getAlliedBankAccountsTest_positive() {
        assertEquals(1, BankAllianceRepository
                .getInstance().getAlliedBankAccounts("b00").size());
        assertTrue(BankAllianceRepository
                .getInstance().getAlliedBankAccounts("b00").contains("a00"));
    }

    @Test
    public void getAlliedBankAccountsTest_noAllies() {
        assertEquals(0, BankAllianceRepository
                .getInstance().getAlliedBankAccounts("c00").size());
    }

    @Test
    public void getAlliedBankAccountsTest_noSuchAccount() {
        assertNull(BankAllianceRepository
                .getInstance().getAlliedBankAccounts("z00"));
    }
}
