package com.hradovich.persistance;

import com.hradovich.model.MoneyTransfer;
import com.hradovich.model.TransferType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MoneyTransferTest {

    @Test
    public void pushPull_positive() {
        MoneyTransferQueue q = MoneyTransferQueue.getInstance();
        MoneyTransfer t = new MoneyTransfer();
        q.push(TransferType.INTRA_BANK.toString(), t);
        assertEquals(t, q.pull(TransferType.INTRA_BANK.toString()));
    }

    @Test
    public void pull_empty() {
        MoneyTransferQueue q = MoneyTransferQueue.getInstance();
        assertNull(q.pull("z"));
    }
}
