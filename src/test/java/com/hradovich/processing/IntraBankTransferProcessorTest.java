package com.hradovich.processing;

import com.hradovich.model.MoneyTransfer;
import com.hradovich.model.TransferRecord;
import com.hradovich.model.TransferRecordType;
import com.hradovich.model.TransferType;
import com.hradovich.persistance.BankMappingRepository;
import com.hradovich.persistance.MoneyTransferQueue;
import com.hradovich.persistance.TransferRecordRepository;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class IntraBankTransferProcessorTest {
    private BankMappingRepository bankMappingRepository = BankMappingRepository.getInstance();
    private TransferRecordRepository transferRecordRepository = TransferRecordRepository.getInstance();

    @Before
    public void before() {
        bankMappingRepository.reset();
        transferRecordRepository.reset();
    }

    @Test
    public void processTransferTest_positive() {
        IntraBankTransferProcessor processor = new IntraBankTransferProcessor();
        transferRecordRepository = TransferRecordRepository.getInstance();
        MoneyTransfer transfer = new MoneyTransfer(
                20L,
                "a01",
                "a02");
        processor.processTransfer(transfer);

        TransferRecord record1 = transferRecordRepository.readRecord(1L);
        TransferRecord record2 = transferRecordRepository.readRecord(2L);

        assertNotNull(record1);
        assertNotNull(record1.getTimestamp());
        assertEquals("a01", record1.getLeft());
        assertEquals("a02", record1.getRight());
        assertEquals(TransferRecordType.DEBIT, record1.getRecordType());
        assertEquals(BigInteger.valueOf(20), record1.getAmount());

        assertNotNull(record2);
        assertNotNull(record2.getTimestamp());
        assertEquals("a02", record2.getLeft());
        assertEquals("a01", record2.getRight());
        assertEquals(TransferRecordType.CREDIT, record2.getRecordType());
        assertEquals(BigInteger.valueOf(20), record2.getAmount());
    }

    @Test
    public void processTransferTest_pushNextLayer() {
        IntraBankTransferProcessor processor = new IntraBankTransferProcessor();
        transferRecordRepository = TransferRecordRepository.getInstance();
        MoneyTransfer transfer = new MoneyTransfer(
                20L,
                "a01",
                "b01");
        processor.processTransfer(transfer);

        assertEquals(transfer,
                MoneyTransferQueue.getInstance()
                        .pull(TransferType.INTER_BANK_ONE_HOP.name()));
    }
}
