package com.hradovich.processing;

import com.hradovich.model.MoneyTransfer;
import com.hradovich.model.TransferRecord;
import com.hradovich.model.TransferRecordType;
import com.hradovich.model.TransferType;
import com.hradovich.persistance.BankMappingRepository;
import com.hradovich.persistance.MoneyTransferQueue;
import com.hradovich.persistance.TransferRecordRepository;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class InterBankOneHopTransferProcessorTest {
    private BankMappingRepository bankMappingRepository = BankMappingRepository.getInstance();
    private TransferRecordRepository transferRecordRepository = TransferRecordRepository.getInstance();

    @Before
    public void before() {
        bankMappingRepository.reset();
        transferRecordRepository.reset();
    }

    @Test
    public void processTransferTest_positive() {
        InterBankOneHopTransferProcessor processor = new InterBankOneHopTransferProcessor();
        transferRecordRepository = TransferRecordRepository.getInstance();
        MoneyTransfer transfer = new MoneyTransfer(
                20L,
                "a01",
                "b01");
        processor.processTransfer(transfer);

        TransferRecord record1 = transferRecordRepository.readRecord(1L);
        TransferRecord record2 = transferRecordRepository.readRecord(2L);
        TransferRecord record3 = transferRecordRepository.readRecord(3L);

        assertRecord(record1, "a00", "a01", TransferRecordType.DEBIT, BigInteger.valueOf(20));
        assertRecord(record2, "a00", "b00", TransferRecordType.CREDIT, BigInteger.valueOf(20));
        assertRecord(record3, "b00", "b01", TransferRecordType.CREDIT, BigInteger.valueOf(20));
    }

    @Test
    public void processTransferTest_pushNextLayer() {
        InterBankOneHopTransferProcessor processor = new InterBankOneHopTransferProcessor();
        transferRecordRepository = TransferRecordRepository.getInstance();
        MoneyTransfer transfer = new MoneyTransfer(
                20L,
                "b01",
                "c01");

        processor.processTransfer(transfer);

        assertEquals(transfer,
                MoneyTransferQueue.getInstance()
                        .pull(TransferType.INTER_BANK_TWO_HOPS.name()));
    }

    private void assertRecord(TransferRecord record, String left, String right,
                              TransferRecordType type, BigInteger amount) {
        assertNotNull(record);
        assertNotNull(record.getTimestamp());
        assertEquals(left, record.getLeft());
        assertEquals(right, record.getRight());
        assertEquals(type, record.getRecordType());
        assertEquals(amount, record.getAmount());
    }
}
