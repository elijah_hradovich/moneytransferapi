package com.hradovich.service;

import act.controller.Controller;
import com.hradovich.controller.MoneyTransferController;
import com.hradovich.controller.UserAccountsController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class MoneyTransferServiceTest {

    @Test
    public void transferTest_positive() {
        MoneyTransferController transferController = Mockito.mock(MoneyTransferController.class);
        UserAccountsController accountController = Mockito.mock(UserAccountsController.class);

        Mockito.when(transferController.checkIfValid(Mockito.any())).thenReturn(true);

        MoneyTransferService service = new MoneyTransferService(transferController, accountController);
        assertEquals(Controller.Util.OK_JSON, service.transfer(20L, "a01", "a02"));
    }

    @Test
    public void transferTest_invalidMessage() {
        MoneyTransferController transferController = Mockito.mock(MoneyTransferController.class);
        UserAccountsController accountController = Mockito.mock(UserAccountsController.class);

        Mockito.when(transferController.checkIfValid(Mockito.any())).thenReturn(false);

        MoneyTransferService service = new MoneyTransferService(transferController, accountController);
        assertEquals(Controller.Util.NO_CONTENT, service.transfer(20L, "a01", "a02"));
    }

    @Test
    public void transferTest_whenError() {
        MoneyTransferController transferController = Mockito.mock(MoneyTransferController.class);
        UserAccountsController accountController = Mockito.mock(UserAccountsController.class);

        Mockito.when(transferController.checkIfValid(Mockito.any()))
                .thenReturn(true);
        Mockito.doThrow(new RuntimeException())
                .when(accountController)
                .updateUserAccountStatesByTransfer(Mockito.any());

        MoneyTransferService service = new MoneyTransferService(transferController, accountController);
        assertEquals(Controller.Util.NO_CONTENT, service.transfer(20L, "a01", "a02"));
    }

    @Test
    public void initTest_positive() {
        MoneyTransferController transferController = Mockito.mock(MoneyTransferController.class);
        UserAccountsController accountController = Mockito.mock(UserAccountsController.class);

        MoneyTransferService service = new MoneyTransferService(transferController, accountController);
        assertEquals(Controller.Util.OK_JSON, service.initListeners());
    }
}
