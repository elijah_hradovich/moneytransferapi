package com.hradovich.virtual;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class VirtualDocumentDatabaseTest {
    private VirtualDocumentDatabase<String> db;

    @Before
    public void before() {
        db = new VirtualDocumentDatabase<>();
        db.insertRow("1", "a");
        db.insertRow("2", "b");
    }

    @Test
    public void getRowTest_positive() {
        assertEquals("b", db.getRow("2"));
    }

    @Test
    public void getRowTest_noSuchKey() {
        assertNull(db.getRow("3"));
    }

    @Test
    public void insertRowTest_positive() {
        db.insertRow("3", "c");
        assertEquals("c", db.getRow("3"));
    }

    @Test
    public void insertRowTest_overwrite() {
        db.insertRow("2", "c");
        assertEquals("c", db.getRow("2"));
    }

    @Test(expected = NullPointerException.class)
    public void insertRowTest_nullKey() {
        db.insertRow(null, "c");
    }

    @Test
    public void deleteRow_positive() {
        db.deleteRow("2");
        assertNull(db.getRow("2"));
    }

    @Test
    public void deleteRow_nonExisting() {
        db.deleteRow("3");
    }

    @Test(expected = NullPointerException.class)
    public void deleteRowTest_nullKey() {
        db.deleteRow(null);
    }
}
