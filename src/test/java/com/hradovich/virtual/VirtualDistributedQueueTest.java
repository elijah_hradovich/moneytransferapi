package com.hradovich.virtual;

import org.junit.After;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class VirtualDistributedQueueTest {
    private VirtualDistributedQueue<String> q;

    @Test
    public void initializeTest_positive() {
        List<String> topics = Arrays.asList("a", "b");

        q = new VirtualDistributedQueue<>(topics);

        assertEquals(q.getTopicNames().size(), 2);
        assertTrue(q.getTopicNames().contains("a"));
        assertTrue(q.getTopicNames().contains("b"));
    }

    @Test
    public void initializeTest_empty() {
        List<String> topics = new LinkedList<>();
        q = new VirtualDistributedQueue<>(topics);
        assertEquals(q.getTopicNames().size(), 0);
    }

    @Test
    public void initializeTest_null() {
        List<String> topics = new LinkedList<>();
        q = new VirtualDistributedQueue<>(topics);
        assertEquals(q.getTopicNames().size(), 0);
    }

    @Test
    public void pushPullTest_positive() throws VirtualDistributedQueueException {
        List<String> topics = Arrays.asList("a", "b");

        q = new VirtualDistributedQueue<>(topics);

        q.push("a", "X");
        assertEquals("X", q.pull("a"));
    }

    @Test
    public void pullEmptyQueueTest_positive() throws VirtualDistributedQueueException {
        List<String> topics = Arrays.asList("a", "b");

        q = new VirtualDistributedQueue<>(topics);

        assertNull(q.pull("a"));
    }

    @Test(expected = VirtualDistributedQueueException.class)
    public void pushTest_negative() throws VirtualDistributedQueueException {
        List<String> topics = Arrays.asList("a", "b");

        q = new VirtualDistributedQueue<>(topics);

        q.push("c", "X");
    }

    @Test(expected = VirtualDistributedQueueException.class)
    public void pullTest_negative() throws VirtualDistributedQueueException {
        List<String> topics = Arrays.asList("a", "b");

        q = new VirtualDistributedQueue<>(topics);

        q.pull("c");
    }

    @Test(expected = VirtualDistributedQueueException.class)
    public void pushNullTopicTest_negative() throws VirtualDistributedQueueException {
        List<String> topics = Arrays.asList("a", "b", "null");

        q = new VirtualDistributedQueue<>(topics);

        q.push(null, "X");
    }

    @Test(expected = VirtualDistributedQueueException.class)
    public void pullNullTopicTest_negative() throws VirtualDistributedQueueException {
        List<String> topics = Arrays.asList("a", "b", "null");

        q = new VirtualDistributedQueue<>(topics);

        q.pull(null);
    }
}
