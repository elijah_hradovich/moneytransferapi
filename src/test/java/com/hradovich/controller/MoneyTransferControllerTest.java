package com.hradovich.controller;

import com.hradovich.model.MoneyTransfer;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MoneyTransferControllerTest {
    //bank accounts one-sided alliances
    //a00 -> b00;
    //b00 -> c00

    @Test
    public void checkIfValid_positive() {
        MoneyTransfer transfer = new MoneyTransfer(
                20L,
                "a01",
                "a02");
        assertTrue(MoneyTransferController
                .getInstance()
                .checkIfValid(transfer));
    }

    @Test
    public void checkIfValid_nullTransfer() {
        assertFalse(MoneyTransferController
                .getInstance()
                .checkIfValid(null));
    }

    @Test(expected = NullPointerException.class)
    public void checkIfValid_nullAmount() {
        MoneyTransfer transfer = new MoneyTransfer(
                null,
                "a01",
                "a02");
        assertFalse(MoneyTransferController
                .getInstance()
                .checkIfValid(transfer));
    }

    @Test
    public void checkIfValid_nullSender() {
        MoneyTransfer transfer = new MoneyTransfer(
                20L,
                null,
                "a02");
        assertFalse(MoneyTransferController
                .getInstance()
                .checkIfValid(transfer));
    }

    @Test
    public void checkIfValid_nullReceiver() {
        MoneyTransfer transfer = new MoneyTransfer(
                20L,
                "a01",
                null);
        assertFalse(MoneyTransferController
                .getInstance()
                .checkIfValid(transfer));
    }
}
