package com.hradovich.controller;

import com.hradovich.model.MoneyTransfer;
import com.hradovich.persistance.UserAccountsRepository;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class UserAccountControllerTest {
    private UserAccountsRepository userAccountsRepository = UserAccountsRepository.getInstance();

    @Before
    public void before() {
        userAccountsRepository.reset();
    }

    @Test
    public void updateUserAccountStatesByTransferTest_bothAccountsExist() {
        MoneyTransfer transfer = new MoneyTransfer(
                20L,
                "a01",
                "a02");
        UserAccountsController.getInstance()
                .updateUserAccountStatesByTransfer(transfer);

        assertEquals(BigInteger.valueOf(980),
                UserAccountsRepository.getInstance()
                        .getAccountStatus("a01"));
        assertEquals(BigInteger.valueOf(1020),
                UserAccountsRepository.getInstance()
                        .getAccountStatus("a02"));
    }

    @Test
    public void updateUserAccountStatesByTransferTest_leftAccountExist() {
        MoneyTransfer transfer = new MoneyTransfer(
                20L,
                "a01",
                "d02");
        UserAccountsController.getInstance()
                .updateUserAccountStatesByTransfer(transfer);

        assertEquals(BigInteger.valueOf(980),
                UserAccountsRepository.getInstance()
                        .getAccountStatus("a01"));
    }

    @Test(expected = NullPointerException.class)
    public void updateUserAccountStatesByTransferTest_rightNull_leftAccountExist() {
        MoneyTransfer transfer = new MoneyTransfer(
                20L,
                "a01",
                null);
        UserAccountsController.getInstance()
                .updateUserAccountStatesByTransfer(transfer);
    }

    @Test
    public void updateUserAccountStatesByTransferTest_rightAccountExist() {
        MoneyTransfer transfer = new MoneyTransfer(
                20L,
                "d01",
                "a02");
        UserAccountsController.getInstance()
                .updateUserAccountStatesByTransfer(transfer);

        assertEquals(BigInteger.valueOf(1020),
                UserAccountsRepository.getInstance()
                        .getAccountStatus("a02"));
    }

    @Test(expected = NullPointerException.class)
    public void updateUserAccountStatesByTransferTest_leftNull_rightAccountExist() {
        MoneyTransfer transfer = new MoneyTransfer(
                20L,
                null,
                "a02");
        UserAccountsController.getInstance()
                .updateUserAccountStatesByTransfer(transfer);
    }
}
