# Run
Run with: mvn clean compile act:run

# Workflow
- MoneyTransfer object arrives to endpoint and is checked to be valid 
- User Accounts are charged if transfer is valid (similarly as blockade on payment cards)
- Transfer is pushed to a distributed queue to be "compiled" into debit/credit records 
- Topics consumers process the transfer in the order: intra -> inter one hop -> inter two hops -> international (not implemented)
- Topic consumer attempts to process the transfer on its level, and if does not have required informations pushes its down the topic
- Cron service synchronize debit/credit records with corresponding banks/systems (not implemented)

# Architectures modeled after
Serverless (on AWS - API Gateway, SQS, Lambda, DynamoDB, Aurora):
- Client device encrypts transfer message and POST it
- API Gateway receive POST and triggers Cloud Function
- Cloud Function decrypts the message and puts it into distributed queue (AWS SQS, Kafka, etc)
- Distributed Queue triggers another Cloud Function, this function transforms the transfer into the series of debit/credit records
- The function writes debit/credit records into document based database, at the same time updating our user account states

Server:
- Loadbalancer before autp-caling group of machines with API endpoints
- Microservices running on auto-scaling grouped by topics machines, polling from the queue

Database:
- We don't want to remove debit/credit records, with their number growing we would prefare a scaling-out instead of scaling-up database
- Only bank accounts and bank relationships would make sense to put into relational-based database, as their number is small and some business logic I don't know about can be involved. But several document-based databases are created instead for the need of this task
- Accounts (User and Bank) - account number:value. No practical use for bank account though, as I miss business logic knowledge of how transfer records and banks are synchronized, as well as how to treat bank account size overall  
- BankMapping - account prefix:bank account number (it is assumed that we can get what bank an account belongs to by account number prefix)
- BankAlliance - account number:account number (many to many relationship, as it is assumed that relationship is of a single direction)
- TransferRecord - id: type, account from, account to, timestamp (type is debit/credit)

# API
http://localhost:5460

GET /records (test-only: returns all transfer records)
GET /user_accounts (test-only: returns all account states)
GET /bank_accounts (test-only: returns all account states)
POST /init (for API framework technical reasons queue consumers need to be initialized by API call, in current implementation each call creates more consumers)
POST /transfer?amount=40&sender=a01&receiver=a02 (for technical reasons currently not a JSON (potentially end-to-end encrypted) is send but basic fields)

# Default Data
- Account number looks like "a01", where "a" is a bank prefix. Bank accounts are like "a00"
- UserAccounts: "a01", "a02", "b01", "c01"
- BankAccounts: "a00", "b00", "c00"
- BankMappingsByPrefix: "a"->"a00", "b"->"b00", "c"->"c00"
- BankAlliances: a00 -> b00; a00 -> c00; b00 -> a00
- TransferRecors: empty

# Code Structure
- service package: restful endpoints
- processing package: topic consumer services of money transfer records
- virtual package: simulation of life distributed tools/databases, used by other layers
- repositories package: data access level
- model package: entities used within the API
- controllers package: controllers working on entities

# Tests
- Unit tests are created with TDD, due to time limitations the coverage is much lower than I would like to
- API (integrational tests) not implemented - while the chosen framework (Act) has claimed a possibility of automated API tests, implementation of them with relyable effects would require more timr than I currently have
- API has been tested with Postman
- mvn clean compile act:run
- GET http://localhost:5460/records (empty)
- GET http://localhost:5460/user_accounts

`[
    {
        "key": "a01",
        "value": 1000
    },
    {
        "key": "a02",
        "value": 1000
    },
    {
        "key": "b01",
        "value": 900
    },
    {
        "key": "c01",
        "value": 1100
    }
]`
- POST http://localhost:5460/transfer?amount=20&sender=a01&receiver=a02 
```
Queue size increase: INTRA_BANK 1
/transfer: a01->a02
```
- POST http://localhost:5460/transfer?amount=20&sender=a01&receiver=b01 
```
Queue size increase: INTRA_BANK 2
/transfer: a01->b01
```
- POST http://localhost:5460/init 
```
Queue size decrease: INTRA_BANK 1
INTRA BANK PROCESSOR: new transfer
DEBIT a01->a02 [100$] 1562847171080
CREDIT a02->a01 [100$] 1562847171080
Queue size decrease: INTRA_BANK 0
INTRA BANK PROCESSOR: not Intra type, push to next layer
Queue size increase: INTER_BANK_ONE_HOP 1
Queue size decrease: INTER_BANK_ONE_HOP 0
INTER BANK PROCESSOR ONE HOP: new transfer
DEBIT a00->a01 [100$] 1562847173081
CREDIT a00->b00 [100$] 1562847173081
CREDIT b00->b01 [100$] 1562847173081
```
- GET http://localhost:5460/records
`
[
    {
        "amount": 100,
        "left": "a01",
        "recordType": "DEBIT",
        "right": "a02",
        "timestamp": 1562847171080
    },
    {
        "amount": 100,
        "left": "a02",
        "recordType": "CREDIT",
        "right": "a01",
        "timestamp": 1562847171080
    },
    {
        "amount": 100,
        "left": "a00",
        "recordType": "DEBIT",
        "right": "a01",
        "timestamp": 1562847173081
    },
    {
        "amount": 100,
        "left": "a00",
        "recordType": "CREDIT",
        "right": "b00",
        "timestamp": 1562847173081
    },
    {
        "amount": 100,
        "left": "b00",
        "recordType": "CREDIT",
        "right": "b01",
        "timestamp": 1562847173081
    }
]
`
- GET http://localhost:5460/user_accounts 

`
[
    {
        "key": "a01",
        "value": 800
    },
    {
        "key": "a02",
        "value": 1100
    },
    {
        "key": "b01",
        "value": 1100
    },
    {
        "key": "c01",
        "value": 1000
    }
]
`
- POST http://localhost:5460/transfer?amount=20&sender=b01&receiver=c01 
```
/transfer: b01->c01
Queue size decrease: INTRA_BANK 0
INTRA BANK PROCESSOR: not Intra type, push to next layer
Queue size increase: INTER_BANK_ONE_HOP 1
Queue size decrease: INTER_BANK_ONE_HOP 0
INTER BANK PROCESSOR ONE HOP: not that type, push to next layer
Queue size increase: INTER_BANK_TWO_HOPS 1
Queue size decrease: INTER_BANK_TWO_HOPS 0
INTER BANK PROCESSOR TWO HOP: new transfer
DEBIT b00->b01 [100$] 1562847395118
DEBIT a00->b00 [100$] 1562847395118
CREDIT a00->c00 [100$] 1562847395118
CREDIT c00->c01 [100$] 1562847395118
```
- POST http://localhost:5460/transfer?amount=20&sender=c01&receiver=b01 
```
/transfer: c01->b01
Queue size decrease: INTRA_BANK 0
INTRA BANK PROCESSOR: not Intra type, push to next layer
Queue size increase: INTER_BANK_ONE_HOP 1
Queue size decrease: INTER_BANK_ONE_HOP 0
INTER BANK PROCESSOR ONE HOP: not that type, push to next layer
Queue size increase: INTER_BANK_TWO_HOPS 1
Queue size decrease: INTER_BANK_TWO_HOPS 0
INTER BANK PROCESSOR TWO HOP: not that type, push to next layer
Queue size increase: INTERNATIONAL 1
```

# Next Steps
- The development was done "in width" that is starting from the single /transfer entry point, the thinking was what to add next
- This includes skipping .yaml file for default data, as it is assumed that time spent on API expansion is more relevant
- This includes skipping endpoint integration tests, as spending a lot of time on "yet another framework" learning how to handle it, when there is no practical value of it - as most certainly this framework its not what is used on production. Estimation is that it would take several days to do anything relevant, so it was passed on
- International transfer consumer is not implemented, because it would be also expansion into currencies as well as it involves a lot of unknown business logic
- Currencies for Intra/Inter bank transfers, but require business logic how it is done practically (bank API call? Are records are stored differently for different currencies - should be so, but difficult to tell and a lot of implementation, etc.)
- New model class for money transfer including sender/receiver banks can be introduced and created on the entrypoint level, once transfer is validated - to save from unneccesary repository calls on different processor levels
- Bank Accounts, bank alliences and other related business logic knowledge is not suffiecient to do data aggregation, but almost certainly this data is better to go to relational database, as banks number would be measured in thousands and most of the information won't be changing that fast

# Development log
- Search for information about how money transfer works. Useful link found (https://blog.revolut.com/how-your-money-moves-around-the-world/)
- Plan the development: the first stage is to create a single endpoint which would process the requiest for transfer, with logic embedded in code
- Architecture choices. There are different types of money transfer which should be handled separately. One way to do it is to have several endpoints, one for each type. However that would been that the layer above should be able to assign proper endpoint and that each endpoint should be load-balanced separately. Another approach is to have a single endpoint for money transfer, which can be made serverles in the cloud or being scaled-out with thgroutput requirements. Upon request a function to classify the transfer type (lets assume by left numbers) is called. Once transfer is classified it would be pushed to distributed queue. Queue consumers would poll the queue and process clasified requests accordingly.
- First stage task. Create domain class for transfer, endpoint, virtual queue, thread-consumers of the queue for Intranational and International transfers. Virtual queue is a mock for standalone application, as AWS SQS or Kafka could not be used. Threads model multi-machine environment.
- Act library is currently chosen as a basis for RESTful: it bases upon main() function call, what is convinient for the task and is lightweight
- Act library after a thought has shown its limitations, as I could not easiely return 400 and 500 messages from the endpoint.
- TransferProcessor idea is introduced. Generally each type of transfer runs on separate instance (or lambda/cloud function) being a consumer of the distributed queue. Since it should be one map and I've already chosen the model pass - consumers are separate threads pulling from the queue on specific topics. Normally there should be a locking mechanism, where lock on transfer would be set on entrypoint and released when transaction is succesful - as queue may deliver messages more than once or processing machine might fail. But for now I skip the lock and work on generalities.
- TransferProcessor now requires accounts, so the transfer would be actually accomplished. And I came close to chosing the database and its structure. Since we are not a bank, all of the accounts most probably are accessed through some kind of bank API. This approach would mean that we keep a record only of transfers themselves, and by simplicity of data distributed database would be the most appropriet - like DynamoDb, Mongo etc. In this model we store a transfer with its status, call users bank API with user token requesting a transaction and provide user with the responce. But this would actually mean that we don't do anything but the bank handles transfer, what is most probably not the idea.
- So lets say that we are a bank and have our own pool of money. This would allow to think about self as of a bank, and implement basing on information found. For our simplistic model we would need accounts (which are ID and value) for INTRA, mapping of external accounts ranges (prefix assumption?) to bank accounts for INTER. All of the rest of transfers should go to international money transfer systems. So two existing data sets are flat and document noSQL database would be the best choice. The easiest way to model it would be to create a Virtual NoSQL database with hash map inside. To summarize the plan is: to implement virtual DB, hardcode some default values into two tables [left: value], [left prefix: left]. After that INTRA transfer can be handled (mapped to some special name), INTER transfer would require access to our vault and can be done in the next step.
- Current solution does not feel full and with reasonable enough assumptions. So now lets assume that we have left value pairs for people from different banks, and when transfer occurs what we do is we save the list of debits and credits and assume that there is a cron which would send it accordingly once in a time (call banks API or some other method). Additionally we would keep a table of bank relations, were key would be a bank left and value would be the ally bank left number. Of course it is many-to-many so a thought to be given to actual implementation. In life bank-accounts only related relationships can be done on SQL database, as they are not many. In this case I still need to do some research about actual implementation (use h2 or do it easy way). THe more important thing is, that those new assumptions allow us to do intra transfer for different banks and allow for inter transfers for non-alligned banks.
- New endpoints added for testing purposes. Integration testing with act was not as easy as promoted. Threads topic-consumers started from main method fail to use the same instance of the queue, probably connected to framework implementation of context and would require more time to solve in beautiful way, /init endpoint is introduced to start consumers manually. Readme expanded
- Since deadline prooved to be more flexible, several architectural things I think of being important at the moment are: synchronous account update (for users who use our app we can use something like money blockade of payment cards), as we know that it is a matter of time that this would happen. And to use the distributed queue for creating debit/credit records which are later synchronized with all of the transaction sides. Another one is to have a separate table for bank accounts. As of the transfer processing itself, I don't like that some things are processed twice (one/two hop inter) and the switch in the begining. So the idea is that we would put it from one topic to another one (check if intra, if so create records, if not - push to inter one hop etc), so the topic itself of where the transfer is holds the information about its state (if it is in inter it means that its not intra, if it is in international it means that we have already checked that it is not intra or inter).
- From implementational point of view, the biggest disadvantages of current solution are: lack of integrational tests (send information to the endpoint and to check what changes it cause on the backend in test data set), the default data is hardcoded instead of being loaded from resources yaml or properties files. From data integrity and databases points of view, bank information is certain to grow (probably there are different API defined per bank, etc), so implementing Bank entity probably should be done in relational way (rare changes to it, not that much data, mostly reads and read-replicas should be able to handle it fine, together with cashing). The question is where to stop for the scope of this task. For now I think it is better to make general cleanup and architectural fixes, run more tests and I'll see how much more time I shall have left.
- Readme updated, as there are all the time next visible steps and next layer of growing "in width" is huge and far beyound the time we have for it, I think that current solution is sufficient